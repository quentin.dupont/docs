# How to…?

## How to change language?

To change default language, you have to:

  1. move your mouse at the bottom of the screen
  * click <i class="fa fa-ellipsis-v" aria-hidden="true"></i> (**1**)
  * click **Paramètres** (**2**):

  ![Framatalk menu image](../../fr/jitsimeet/images/framatalk_menu.png)
  * click **Plus**
  * change **Langue** to another one:

  ![Framatalk paramètres plus image](../../fr/jitsimeet/images/Framatalk_parametres_plus.png)
  * click **Ok**
