# Synchronisation sur iOS
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

## Agendas
* Ouvrez l'application **Réglages**.
* Sélectionnez **Mots de passe et comptes**.
* Sélectionnez **Ajouter un compte...**
* Sélectionnez **Autre**
* Sélectionnez **Ajouter un compte CalDAV**.
* Entrez les détails de votre compte.

    * Pour le serveur, entrez l'URL du type `https://framagenda.org/remote.php/dav/principals/users/utilisateur/` visible dans les paramètres de Framagenda (en bas à gauche).
    ![Adresse CalDAV iOS](../images/agenda-5.png)
    * Entrez votre nom d'utilisateur et votre mot de passe.
    * Sélectionnez **Suivant**.
* Si les informations sont correctes, sélectionnez **Terminé**.

Votre agenda sera maintenant synchronisé et visible dans l'application Calendrier.

<p class="alert-info alert">
<b>Note</b> : Si vous utilisez <a href="../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa">l'authentification en deux étapes</a>, vous devez créer <a href="../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication">un mot de passe d'application</a>.
</p>

## Contacts
* Ouvrez l'application **Réglages**.
* Sélectionnez **Mots de passe et comptes**.
* Sélectionnez **Ajouter un compte...**
* Sélectionnez **Autre**
* Sélectionnez **Ajouter un compte CardDAV**.
* De la même manière que pour les agendas, entrez les détails de votre compte.
* Si les informations sont correctes, sélectionnez **Terminé**.

Vos contacts seront maintenant synchronisés et visibles dans l'application Contacts.
