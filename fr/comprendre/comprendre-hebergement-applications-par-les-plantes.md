# Comprendre l’hébergement d’application par les plantes

C’est dans une mailing-liste ou un commentaire par un membre de Framasoft que j’ai trouvé cette image que je reprends pour moi, tant je la trouve bien. Elle permet de comprendre et faire comprendre les différentes façons que l’on a de mettre en place une application sur un serveur.
Une connaissance de ce que sont que des serveurs dédiés, mutualisés, docker et les applications logicielles aidera à la compréhension de cette analogie.

Une application, c’est un peu comme une plante que l’on souhaite faire grandir. Il y a différentes façons de le faire.

## Serveur dédié

Mettre une application sur un serveur dédié, c’est un peu comme mettre une plante en pleine terre. La plante aura toute la place qu’elle veut, pourra utiliser toutes les ressources mises à sa disposition et grandir, grandir…

Ce n’est pas forcément ce que l’on souhaite si on veut avoir un minimum de contrôle sur la croissance de la plante et les ressources qu’elle consomme.

## Serveur mutualisé

Dans le cas d’un serveur mutualisé, l’application partage ses ressources avec d’autres plantes, son environnement est contrôlé. On est dans un cas semblable à une installation "en pot" : le jardinier contrôle les apports en nutriments, en eau. Il maintient la plante à une certaine taille via des coupes de branches… La plante est sous contrôle, dans un environnement maîtrisé.

Un peu comme une application sur un serveur mutualisé, est sous contrôle, elle a des ressources limitées et ne peut pas s’étendre.

## Docker

Le cas de Docker est un cas un peu particulier. Dans le cas de Docker, les applications sont dans un environnement qui est mi-chemin entre le serveur dédié et le serveur mutualisé. L’analogie serait de mettre des plantes sous-serre. Les plantes ont plus d’espace pour leur croissance (surtout au niveau des racines) que dans un pot. Mais leur environnement reste maîtrisé, contrôlé (via la serre). La plante peut côtoyer d’autres plantes, leur faire concurrence pour les ressources ou au contraire vivre en symbiose… On est dans un environnement clos, mais plus vaste que celui d’un pot.

Toute comme une application dans un conteneur docker a plus de flexibilité qu’une application en serveur mutualisé, mais on n’est pas pour autant sur un serveur dédié, vu que l’application reste dans un environnement maîtrisé et restreint.
