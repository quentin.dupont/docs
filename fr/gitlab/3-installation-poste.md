# Installation d'un poste de travail

## Ce que nous allons apprendre

*  Installation du logiciel git
*  Préparer identification de son ordinateur pour la communication avec Framagit
*  Initialisation de votre dépôt local
*  Modifier votre premier document

## Procédons par étapes

Nous revenons sur notre ordinateur local.

### 1. Installer Git

En général Git n'est pas installé par défaut sur votre ordinateur.

Pour l'installer sous GNU/Linux (Debian, Mint, Ubuntu, et bien sûr Emmabuntüs) (Pour les autres types de distributions, ou pour les systèmes Windows et MacOSX, voir les wikis respectifs à la section installation de logiciels.), ouvrir un terminal et entrer :

`sudo apt-get install git`

Puis renseigner votre mot de passe et répondre **Oui** pour accepter le téléchargement.


<p class="alert alert-info">
Dans un terminal, sous GNU/Linux lors de la frappe des caractères du mot de passe, ceux-ci ne sont pas visualisés par des * et rien n'est affiché pour des raisons de sécurité. Donc tapez votre mot de passe et ensuite appuyer sur la touche « Entrée »
</p>


### 2. Préparer la connexion avec Framagit

Toujours dans une fenêtre, créer la clé de chiffrement qui va servir aux communications avec notre projet sur le serveur Framagit :

`ssh-keygen`

Taper Entrée sans rien changer aux différentes options proposées.

Afficher la clé dans le terminal :

`cat .ssh/*.pub`

Sélectionner toute la clé, et la copier dans le presse-papier avec un clic-droit &gt; Copier

![](images/copier-cle-ssh.png)

Retourner sur la page « Profile Settings » de votre compte Framagit, et cliquer sur le menu «  **SSH Keys**  » ou sur ce lien direct : [https://framagit.org/profile/keys](https://framagit.org/profile/keys)

![](images/Framagit-SSH-Keys.png)

La fenêtre des clés SSH va s'ouvrir :

![](images/Framagit-Add-Key-1.png)

Et l'on va coller la clé SSH dans la boite du champ « _Key_ »en faisant un clic-droit &gt; Coller.

On peut aussi renseigner le titre de cette nouvelle clé dans le champ « _Title_ »

puis on clique sur le bouton vert «  **Add Key**  »

![](images/Framagit-Add-Key-2.png)

On reste sur Framagit mais on retourne sur la page du projet « Communication ».

![](images/Framagit-copy-url.png)

Sous le titre du projet « Communication » on choisit l'option de connexion [SSH](https://fr.wikipedia.org/wiki/Secure_Shell) (et non pas [HTTPS](https://fr.wikipedia.org/wiki/HyperText_Transfer_Protocol_Secure)). Et on copie dans le presse-papier l'adresse qui se trouve à droite en cliquant sur l'icône tout à droite « _Copy to Clipboard_ »

### 3. Initialisation du Git local

Maintenant tout est prêt sur le site Framagit, et nous allons finir de paramétrer le Git local.

Toujours dans notre terminal :


`git config --global user.name "Yves Saboret"
git config --global user.email "mettre ici votre adresse mail"`

Créer le dossier dans lequel nous allons cloner le projet Emmabuntüs et y aller. Par exemple :

`mkdir Git-Emma
cd Git-Emma`

Créer le clone du projet « Communication » :

`git clone <copier ici l'adresse URL du projet qui est dans le presse-papier>`

L'opération va débuter par un petit message, qui n'est pas une erreur, et auquel il faut répondre « yes » :

`
Cloning into 'Communication'…
The authenticity of host 'git.framasoft.org (2a01:4f8:200:1302::42)'
can't be established.
ECDSA key fingerprint is SHA256:nO6L2sApWj/OkjW7avditV/dHOMEG/cV7Ps5z7yaS30.
Are you sure you want to continue connecting (yes/no)?** Yes
Warning: Permanently added 'git.framasoft.org,2a01:4f8:200:1302::42'
(ECDSA) to the list of known hosts.**
`

Attention le clonage va prendre un temps certain, mais Git vous tient au courant de l'avancement des travaux.

### 4. Modifier un document

Les opérations décrites précédemment ne sont à faire qu'une seule fois. Maintenant nous allons nous intéresser aux opérations à effectuer lorsqu'on veut modifier un (ou plusieurs) document(s), puis le(s) renvoyer sur le site Framagit.

Et d'abord, partir de maintenant et chaque fois que vous le souhaitez, vous pouvez demander l'état de votre clone en entrant :

`git status`

ou bien

`git status > ../modif.txt`

pour sauver l'état dans le fichier _modif.txt_

ou bien encore

`git status <chemin-à-vérifier>`

Cette commande vous indique si tout est à jour dans votre copie locale

ou bien, au contraire, si des mises à jour sont en attente.

![](images/Git_Status_1.png)

Par exemple, en ce moment je travaille sur ce document, mais je n'ai pas encore dit à Git de l'ajouter dans le projet et il me le signale

Toutefois, la première chose à faire, c'est de vérifier que votre clone local est bien à jour (d'autres personnes ont peut-être modifié quelques fichiers entre-temps). Donc on ré-synchronise avec la commande :

`git pull`

pour être sûr que notre clone est bien à jour par rapport au serveur.

<p class="alert alert-info">
Attention : Avant de modifier les fichiers de type .odt (dans le projet Communication, par exemple) il faut prendre quelques précautions, et avertir la communauté que le fichier va être édité, donc on leur demande de ne pas y toucher. Nous expliciterons comment un peu plus loin.
</p>

Donc vous pouvez modifier un (ou des) fichier(s) en local en vous plaçant à l'endroit adéquat de l'arborescence. Dans notre exemple, nous serions ici :

**~/Git-Com/Communication/Tutoriels/Fr/**

pour y modifier le fichier **DVP\_Presentation\_Framagit.odt**

Une fois que vous êtes satisfait des modifications apportées au(x) fichier(s), il faut mettre à jour l'index de votre branche locale par la commande :

`git add <fichier-modifié>`

Une fois tous les fichiers ajoutés on valide le tout par un « _commit_ », en expliquant le sens ou la raison des  modifications :

`git commit -m "raison de l'évolution"`

Notre exemple :

![](images/Git-Commit.png)

Enfin on peut mettre à jour la branche maîtresse sur le serveur Framagit :

`git push -u origin master`

Si malheureusement, le fichier a été modifié entre temps, la commande **git push** retournera une erreur et il faudra recommencer les modifications (après un nouveau **git pull** pour tout mettre à jour).

Par contre, si tout se passe bien, on peut informer la communauté que votre travail est fini, et donc que quelqu'un d'autre peut éventuellement prendre le relais.


<p class="alert alert-info">
Note : ce problème avec les mises à jour simultanées provient du fait que les fichiers de type .odt sont compressés (binaires) et non pas de simples fichiers textes comme les codes source, et Git ne sait pas faire la réconciliation automatique des modifications simultanées sur de tels fichiers.
</p>

----

## Faisons le point

Maintenant que vous savez envoyer votre premier document, nous allons voir [comment gérer la communication au sein des membres de l'équipe](4-gerer-communication.html).
