# Export de la base de données

Pour exporter les données de votre espace Framaboard vous devez vous rendre dans vos préférences (en cliquant sur votre avatar > **Préférences**), puis cliquer sur **Télécharger la base de données (Fichier Sqlite compressé en Gzip)**

![Image export données](screenshots/kanboard-export-donnees.png)
