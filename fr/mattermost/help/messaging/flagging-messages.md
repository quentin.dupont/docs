# Marquer un message

Framateam vous permet de marquer un message pour pouvoir le retrouver plus facilement (une information importante par exemple).

![gif montrant comment marquer un message](../../images/team-flag-message.gif)

## Marquer un message d'un indicateur de suivi

Pour marquer un message vous devez passer la souris dessus et cliquer sur l'icône <i class="fa fa-flag-o" aria-hidden="true"></i>

## Retrouver un message marqué

Pour retrouver la liste des messages marqués vous devez cliquer sur l'icône <i class="fa fa-flag-o" aria-hidden="true"></i> à côté de la barre de recherche (tout en haut à droite de l'écran).

![image montrant l'icône pour retrouver les messages marqués](../../images/team-flag-search.png)

## Supprimer un indicateur

Pour enlever l'indicateur de suivi vous devez cliquer sur l'icône <i class="fa fa-flag" aria-hidden="true"></i> du message.
