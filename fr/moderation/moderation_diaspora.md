# Modération sur diaspora* (Framasphère)

## Vous protéger rapidement

### Masquer ou ignorer un contenu

Vous pouvez masquer <i class="fa fa-times" aria-hidden="true"></i> un contenu grâce à l’icône située en haut à droite (affichée au survol).

![image montrant comment masquer un post](images/diaspora_moderation_masquer_post.png)

### Ignorer une personne

Vous pouvez également ignorer <i class="fa fa-ban" aria-hidden="true"></i> une personne (ne plus voir son contenu) grâce&nbsp;:

  * à l’icône située en haut à droite d’un contenu (affichée au survol),
  * à l’icône située en bas à droite de l’entête de son profil.

![image montrant comment ignorer un compte depuis un post](images/diaspora_moderation_ignorer_post.png)

![image montrant comment ignorer un compte depuis un profil](images/diaspora_moderation_ignorer_post2.png)


## Signaler à l’équipe de modération

### Signaler un contenu

Vous pouvez signaler un contenu grâce à l’icône <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> située en haut à droite (affichée au survol).

![image montrant comment signaler un post](images/diaspora_moderation_signaler_post.png)

Vous accédez ensuite à une fenêtre vous demandant de préciser en quoi le contenu enfreint <a href="https://framasoft.org/fr/moderation/" title="lien vers la charte de modération">la charte de modération</a>.

![image montrant la fenêtre de signalement d'un post](images/diaspora_moderation_signaler_post_popup.png)

Votre signalement sera ensuite envoyé à l’équipe de modération bénévole.
