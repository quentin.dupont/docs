# Framabee

Framabee est un métamoteur de recherche regroupant les résultats d'autres moteurs de recherche mais sans conserver d'informations sur les utilisateurs.

Framabee ne vous trace pas, ne partage aucune donnée avec un tiers et ne peut pas être utilisé pour vous compromettre.

## Pour aller plus loin

  * [Prise en mains](prise-en-mains.html)
  * [Utiliser Framabee](https://framabee.org/)
  * Un service proposé dans le cadre de la campagne [Dégooglisons Internet](https://degooglisons-internet.org)
